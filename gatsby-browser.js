require("./src/styles/normalize.scss");
require("bootstrap/dist/css/bootstrap.min.css");
require("./src/styles/main.scss");
require("./src/styles/header.scss");
require("./src/styles/footer.scss");
require("./src/styles/postcard.scss");
require("./src/styles/search.scss");
require("./src/styles/post.scss");
require("./src/styles/404.scss");

