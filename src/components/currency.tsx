import React from "react";

const Currency = ({ gold = 0, silver = 0, bronze = 0 }) => {
  const rawCurrency = gold * 10000 + silver * 100 + bronze;
  const money = [
    { amount: gold.toLocaleString(), icon: "gold-small", alt: "gold" },
    { amount: silver, icon: "silver-small", alt: "silver" },
    { amount: bronze, icon: "bronze-small", alt: "bronze" }
  ];

  if (rawCurrency < 10000) money.shift();
  if (rawCurrency < 100) money.shift();

  return (
    <>
      {money.map(moni => {
        return (
          <span key={moni.alt}>
            {moni.amount}{" "}
            <img src={`/images/${moni.icon}.png`} alt={moni.alt} style={{marginBottom: "4px"}}/>{" "}
          </span>
        );
      })}
    </>
  );
};

export default Currency;
