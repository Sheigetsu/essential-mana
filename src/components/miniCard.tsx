import React from "react";
import { Link } from "gatsby";

const MiniCard = ({ props }) => {
  const desc = props.description.substr(0, 45);
  return (
    <Link to={props.slug}>
      <div className={"miniCard"}>
        <div className={"miniThumb"}>
          <img src={props.thumbnail} alt={props.title} />
        </div>
        <div className={"rest"}>
          <div>
            {props.title} - by {props.author}
          </div>
          {/* <div>{`${desc} ${desc.length === 45 ? "[...]" : ""}`}</div> */}
        </div>
      </div>
    </Link>
  );
};

export default MiniCard;
