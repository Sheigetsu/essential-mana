FROM node:13-buster-slim as build

WORKDIR /app
RUN yarn global add gatsby-cli && gatsby telemetry --disable

ADD . ./
RUN yarn install
RUN gatsby build


FROM abiosoft/caddy:1.0.3-no-stats
RUN echo 'tls off' >> /etc/Caddyfile
COPY --from=build /app/public /srv
