import React, { useState } from "react";
import onClickOutside from "react-onclickoutside";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import MiniCard from "./miniCard";

const Search = ({ items = [] }) => {
  const [open, setOpen] = useState(false);
  const [search, setSearch] = useState("");
  let myInput;

  const toggle = () => {
    setOpen(!open);

    // state is not changed yet so this makes it work
    if (!open) myInput.focus();
  };

  const filtered = items
    .filter(item => item.title.toUpperCase().includes(search.toUpperCase()))
    .slice(0, 5); // hardcoded

  const onFilterChange = event => {
    setSearch(event.target.value);
  };

  Search.handleClickOutside = event => setOpen(false);

  return (
    <div className={"wrap"}>
      <div className="search">
        <button
          className={["searchBtn", open ? "" : "searchBtnClosed"].join(" ")}
          onClick={toggle}
        >
          <FontAwesomeIcon icon={faSearch} />
        </button>
        <input
          type="text"
          className={["searchTerm", open ? "" : "searchTermClosed"].join(" ")}
          placeholder="Search..."
          onChange={onFilterChange}
          ref={ip => (myInput = ip)}
        />
      </div>
      {open && search !== "" && (
        <div className={"results"}>
          <ul className="result-content">
            {filtered.map(item => (
              <li key={item.id}>
                <MiniCard props={item} />
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

const clickOutsideConfig = {
  handleClickOutside: () => Search.handleClickOutside,
};

Search.prototype = {};

export default onClickOutside(Search, clickOutsideConfig);
