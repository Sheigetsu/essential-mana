import React from "react";

const EMItem = (props) => {
    const variant = props.variant ? props.variant : null;
    switch (variant) {
        case 'inline':
            return(
              <a href={"https://teralore.com/en/item/" + props.id} target={"_blank"}>{props.name}</a>
            );
        case 'full':
            return(
              <span style={"error-link"}>FULL ITEM TOOLTIP NOT SUPPORTED YET</span>
            );
        default:
            return(
              <span style={"error-link"}>NO ITEM VARIANT FOUND</span>
            );
    }
};

export default EMItem;
