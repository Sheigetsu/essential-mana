module.exports = {
  pathPrefix: `/essential-mana`,
  siteMetadata: {
    title: `Essential Mana`,
    description: `TERA (Non-official) Fansite`,
    source: `https://github.com/Sheigetsu/essential-mana`,
    author: `Bobbuz`,
    devs: `Sheigetsu & Lloyderino`,
    version: '0.1.0'
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `posts`,
        path: `${__dirname}/src/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        gatsbyRemarkPlugins: [`gatsby-remark-autolink-headers`],
        defaultLayouts: {
          posts: require.resolve("./src/components/layouts/postsLayout.tsx"),
          default: require.resolve("./src/components/layouts/defaultLayout.tsx"),
        }
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Roboto Mono\:400,700`
        ],
        display: 'swap'
      }
    },
    `gatsby-plugin-sass`
  ],
}
