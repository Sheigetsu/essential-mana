# essential-mana

### Project setup
```
npm install
```

### Development server
```
gatsby develop
```

### Mobile Development Server
```
gatsby develop -H <local ip> -p <port>
```

### Build production server
```
gatsby build
```

### Run production build
```
gatsby serve
```