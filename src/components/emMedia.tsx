import React from "react";

export const Image = (props) => {
  const variant = props.variant ? props.variant : null;
  switch (variant) {
    case "thumbnail":
      return (
        <img src={props.source} alt={props.alt} className={"thumbnail"}/>
      );
    default:
      return (
        <div className={props.align ? "image-" + props.align : ""}>
          <img src={props.source} alt={props.alt}/>
        </div>
      );
  }
};

export const Webm = (props) => {
  return (
    <div className={props.align ? "webm-" + props.align : ""}>
      <video controls={true} autoPlay={props.autoPlay} loop={props.loop}>
        <source src={props.source} type={"video/webm"}/>
      </video>
    </div>
  );
};
