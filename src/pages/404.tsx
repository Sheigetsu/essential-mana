import React from "react";
import DefaultLayout from "../components/layouts/defaultLayout";

const NotFoundPage = () => (
    <DefaultLayout>
        <div className={"404-page"}>
            <h2>404 not found</h2>
        </div>
    </DefaultLayout>
);

export default NotFoundPage;
