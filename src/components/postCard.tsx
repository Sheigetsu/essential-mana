import React from "react";
import { Link } from "gatsby";
import { Image } from "./emMedia";

const PostCard = (data) => {
    return (
        <div className={"postCard"}>
            <div className={"header"}>
                <Image source={data.thumbnail} variant={"postCard"}/>
                <h5><Link to={data.slug + "/"}>{data.title}</Link></h5>
                <sub>by {data.author} {data.date ? "- " + data.date : null}</sub>
            </div>
            <div className={"content"}>
                <span>{data.description}</span>
            </div>
        </div>
    );
};

export default PostCard;
