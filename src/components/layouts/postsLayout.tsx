import React from "react";
import { MDXProvider } from "@mdx-js/react";
import EMItem from "../emItem";
import Currency from "../currency";
import Header from "../header";
import Footer from "../footer";
import { graphql } from "gatsby";
import { MDXRenderer } from "gatsby-plugin-mdx";
import Container from "react-bootstrap/Container";
import { Image, Webm } from "../emMedia";

const shortcodes = {
  Currency,
  EMItem,
  Image, Webm
};

const PostsLayout = ({ data: { mdx } }) => {
  const { tableOfContents } = mdx;
  return (
    <>
      <MDXProvider components={shortcodes}>
        <Header/>
        <main>
          <Container>
            <div className={"post-page"}>
              <Image source={mdx.frontmatter.thumbnail} variant={"thumbnail"}/>
              <div className={"post-header"}>
                <h2>{mdx.frontmatter.title}</h2>
                <sub>by {mdx.frontmatter.author} - {mdx.frontmatter.date}</sub>
              </div>
              {typeof tableOfContents.items === "undefined" ? null : (
                <>
                  <hr/>
                  <div className={"toc"}>
                    <h2>Table of Contents</h2>
                    <ul>
                      {tableOfContents.items.map(i => (
                        <li key={i.url}>
                          <a href={i.url} key={i.url}>
                            {i.title}
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                </>
              )}
              <hr/>
              <MDXRenderer>
                {mdx.body}
              </MDXRenderer>
            </div>
          </Container>
        </main>
        <Footer/>
      </MDXProvider>
    </>
  );
};

export default PostsLayout;

export const pageQuery = graphql`
    query PostQuery($id: String) {
        mdx(id: { eq: $id }) {
            id
            body
            tableOfContents
            frontmatter {
                title
                thumbnail
                author
                slug
                date(formatString: "dddd MMMM Do, YYYY")
                tags
            }
        }
    }
`;
